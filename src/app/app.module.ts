import { LoginNamePage } from './../pages/login-name/login-name';
import { LoginPage } from './../pages/login/login';
import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { SpeechRecognition } from '@ionic-native/speech-recognition';
import { Hotspot } from '@ionic-native/hotspot';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { HotspotPage } from '../pages/hotspot/hotspot';


import { Sim } from '@ionic-native/sim';
import { MobileAccessibility } from '@ionic-native/mobile-accessibility';

import { TextToSpeech } from '@ionic-native/text-to-speech';
import { Vibration } from '@ionic-native/vibration';
var config = {   
  backButtonText: '',
  iconMode: 'ios',
  pageTransition: 'ios',
  mode:'ios',
  scrollPadding: false
};

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    HotspotPage,
    LoginPage,
    LoginNamePage,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp,config)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    HotspotPage,
    LoginPage,
    LoginNamePage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    SpeechRecognition,
    Hotspot,
    AndroidPermissions,
     Sim,
     SpeechRecognition,
     MobileAccessibility,
     TextToSpeech,
     Vibration,

    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}

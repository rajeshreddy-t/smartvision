import { MobileAccessibility } from '@ionic-native/mobile-accessibility';
import { SpeechRecognition } from '@ionic-native/speech-recognition';
import { AboutPage } from './../about/about';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController, App } from 'ionic-angular';
import { Vibration } from '@ionic-native/vibration';
import { TextToSpeech } from '@ionic-native/text-to-speech';
import { Promise } from 'q';

/**
 * Generated class for the LoginNamePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login-name',
  templateUrl: 'login-name.html',
})
export class LoginNamePage {
matched_strings:any

talk_back:boolean=false;
  constructor(public navCtrl: NavController,
    private maccess:MobileAccessibility,
    private vibration:Vibration,
    private tts:TextToSpeech,
    private app:App,
    private loadingCtrl:LoadingController ,private toastCtrl:ToastController , private speechrecognition:SpeechRecognition ,public navParams: NavParams) {
     
     
     
      this.maccess.isTalkBackRunning().then(
        (success)=>{
          // this.talk_back=success;
        },
        (err)=>{
           this.presentToast('ms access error is talk back running')
           
        }


      )
  this.getPermission().then(
    (success)=>{
           if(success=='granted')
           {
            if(this.talk_back)
            {
          
            }
            else{
              this.ask_name();
            }
           }
    },
    (err)=>{


    });
  
  
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginNamePage');
  }


/*****asking for name */

ask_name()
{
this.maccess.isTalkBackRunning().then(
  (success)=>{
      if(this.talk_back)
      {
    this.maccess.speak('Hey, can you tell me your name');
      }
      else{
        this.texttospeech('Hey, can you tell me your name');
      }


    this.start_listening();
    
  },
  (err)=>{



  }
)
 
}

/***end of ask name */







  getPermission() {


    return Promise((resolve,reject)=>
    {
  
      this.speechrecognition.hasPermission()
        .then((hasPermission: boolean) => {
          if (!hasPermission) {
            this.texttospeech('please grant the permission to record your voice by clicking on ok ');
            this.speechrecognition.requestPermission().then((success)=>{resolve('haspermission');this.presentToast('granted')},(err)=>{this.presentToast('declined'); this.getPermission();});
            
          }
          else{
            resolve('haspermission');
          }
        });
    })
   
  }
  
confirm_strings:any=['yes','yas','s','yea','yeah','YES','Yes','YEs'];
confirm_said_string:any;
  start_listening()
  {

  //   let options = {
  //   String language,
  //   Number matches,
  //   String prompt,      // Android only
  //   Boolean showPopup,  // Android only
  //   Boolean showPartial 
  // }

  let options={
    language:"en-US",
     matches:5,

  }
 
 this.showLoader('listening....please talk')
    this.speechrecognition.startListening(options)
    .subscribe(
      (matches: Array<string>) => {
        console.log(matches);this.matched_strings=JSON.stringify(matches);this.loading.dismiss()
      
         this.texttospeech('Is your name'+this.matched_strings[0]).then(
           (success)=>{ 

                      this.speechrecognition.startListening(options)
                      .subscribe(
                        (matches: Array<string>) => {  this.confirm_said_string=matches[0].split(' ');
                             
                        this.confirm_strings.forEach(str => {
                          if(str==this.confirm_said_string)
                          {
                              this.texttospeech('thank you '+this.matched_strings[0]+'Welcome to smart vision your setup is done,I will  naviagte you to home screen').then(
                                    (success)=>{this.app.getRootNav().setRoot(AboutPage);},
                                    (err)=>{}
                                    );

                          }
                          else{
                            this.texttospeech('thank you '+this.matched_strings[0]+'Welcome to smart vision your setup is done,I will  naviagte you to home screen').then(
                              (success)=>{this.app.getRootNav().setRoot(AboutPage);},
                              (err)=>{}
                              );
                          }
                          
                        });
                          },
                        (err)=>{}
                        );
               },
           (err)=>{}
           )

        this.maccess.isTalkBackRunning().then(
          (success)=>{
            this.maccess.speak('Hi'+this.matched_strings[0]+'Welcome to smart vision your setup is done,I will  naviagte you to home screen');
            this.app.getRootNav().setRoot(AboutPage);
      },(err)=>{

      }
      )
      },
      (onerror) => {console.log('error:', onerror); this.matched_strings="error=>"+JSON.stringify(onerror);this.loading.dismiss();}
    )
  
  // Stop the recognition process (iOS only)

  }





  next()
  {
    this.app.getRootNav().setRoot(AboutPage);
  }






/**tts general  */

texttospeech(msg)
{

  return Promise((resolve,reject)=>{
    this.tts.speak(msg).then(
      (success)=>{this.presentToast(msg+success); resolve('tts success')},
      (error)=>{this.presentToast(error); resolve('tts declined')}
    )

  });
  
}

/** */

/***toast */
  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 8000,
      position: 'bottom',
      dismissOnPageChange: true
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  /**end of toast */



  loading:any;
  /**loading */
  showLoader(msg){
    this.loading = this.loadingCtrl.create({
        content: msg
    });

    this.loading.present();
  }

  /**end of loading */

}

// import { Hotspot } from 'ionic-native';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { Hotspot } from '@ionic-native/hotspot';

/**
 * Generated class for the HotspotPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-hotspot',
  templateUrl: 'hotspot.html',
})
export class HotspotPage {
hotspot_st:any;
loading:any;
hotspot_ssid:any='smartvision';
hotspot_password:any='smartvision@123';
hotspot_mode:any='Open';

hotspot_return_val:any;
  constructor(public navCtrl: NavController,public toastCtrl:ToastController,public hotspot:Hotspot,public loadingCtrl:LoadingController,public navParams: NavParams) {
  

  this.hotspot_st='off';
  this.On_hotspot();
  }

  showLoader(){
    this.loading = this.loadingCtrl.create({
        content: 'Switching on...'
    });

    this.loading.present();
  }



  On_hotspot()
  {

    this.showLoader();
    this.hotspot.createHotspot(this.hotspot_ssid,this.hotspot_password,this.hotspot_mode).then(
      (success)=>{    this.loading.dismiss(); this.hotspot_st='on'; this.presentToast(JSON.stringify(success))
    this.hotspot_return_val=JSON.stringify(success)},
      (error)=>{     this.loading.dismiss(); this.hotspot_st='off';
      this.hotspot_return_val=JSON.stringify(error);this.presentToast(JSON.stringify('err'+error))}
    )
  }

doRefresh(refresher)
{
  this.hotspot_st='on'
  this.On_hotspot();
if(refresher!=0)    
refresher.complete();
}
  check_connections()
  {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HotspotPage');
  }


  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 8000,
      position: 'bottom',
      dismissOnPageChange: true
    });
  }

}

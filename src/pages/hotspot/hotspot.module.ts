import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HotspotPage } from './hotspot';

@NgModule({
  declarations: [
    HotspotPage,
  ],
  imports: [
    IonicPageModule.forChild(HotspotPage),
  ],
})
export class HotspotPageModule {}

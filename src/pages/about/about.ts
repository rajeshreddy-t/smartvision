import { Component } from '@angular/core';
import { NavController, AlertController, LoadingController } from 'ionic-angular';
import { Hotspot, HotspotNetwork } from '@ionic-native/hotspot';
import { AndroidPermissions } from '@ionic-native/android-permissions';
@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {

  hotspot_stat:any=false;

loading:any;
list:any;
activeSelect:any;

  hotspot_ssid:any='smartvision';
  hotspot_password:any='smartvision@123';
  hotspot_mode:any='Open';
  constructor(public navCtrl: NavController,public loadingCtrl:LoadingController,public alertCtrl:AlertController,private hotspot: Hotspot,public androidPermissions: AndroidPermissions) {
this.createHotspot();
  //   this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_WIFI_STATE).then(
  //     result => console.log('Has permission?',result.hasPermission),
  //     err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.CAMERA)
  //   );

  //   this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.CAMERA, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
   

  this.list=[{img:"assets/icon/mylovedone.png",title_en:"Support",title_ar:"الأطباء",component:''},{img:"assets/icon/caregivercr.png",title_en:"My Contacts",component:'',id:1},{img:"assets/icon/carevisit.png",title_en:"Help",title_ar:"العيادات",component:''},{img:"assets/icon/wellness1.png",title_en:"Feedback",title_ar:"مقالات",component:''}]
  

}


activeItem(index){
  this.activeSelect=index;
}
showLoader(){
  this.loading = this.loadingCtrl.create({
      content: 'Switching on...'
  });

  this.loading.present();
}


doRefresh(refresher)
{
this.createHotspot();
if(refresher!=0)    
refresher.complete();
}


release_hotspot()
{
this.hotspot.createHotspot(this.hotspot_ssid,this.hotspot_password,this.hotspot_mode).then(
  (success)=>{this.showAlert(success)},
  (error)=>{}
)

}

return_of_availability:any;
isavailable()
{this.hotspot.isAvailable().then(
(res)=>{this.return_of_availability=JSON.stringify(res)},
(er)=>{}
)

}




showAlert(msg) {
  const alert = this.alertCtrl.create({
    title: 'hostpot started',
    subTitle: 'result='+msg,
    buttons: ['OK']
  });
  alert.present();
}





return_of_createHotspot:any

createHotspot()
{
  this.showLoader();
  this.hotspot.createHotspot(this.hotspot_ssid,this.hotspot_mode,this.hotspot_password).then(
(res)=>{ this.loading.dismiss();this.return_of_createHotspot=JSON.stringify(res)},
(err)=>{this.loading.dismiss();this.return_of_createHotspot=JSON.stringify(err)}
  )
  
}


return_of_configureHotspot:any;

configureHotspot()
{
  this.hotspot.configureHotspot(this.hotspot_ssid,this.hotspot_mode,this.hotspot_password).then(
    (res)=>{this.return_of_configureHotspot=JSON.stringify(res)},
    (err)=>{this.return_of_configureHotspot=JSON.stringify(err)}
      )
}

return_of_isHotspotEnabled:any;

isHotspotEnabled()
{
  this.hotspot.isHotspotEnabled().then(
    (res)=>{this.return_of_isHotspotEnabled=JSON.stringify(res)},
    (err)=>{this.return_of_isHotspotEnabled=JSON.stringify(err)}
      )
}


return_of_stopHotspot:any;
stopHotspot()
{
this.hotspot.stopHotspot().then(
  (res)=>{this.return_of_stopHotspot=JSON.stringify(res)},
  (err)=>{this.return_of_stopHotspot=JSON.stringify(err)}
)

}



return_of_startHotspot:any;
startHotspot()
{
this.hotspot.startHotspot().then(
  (res)=>{this.return_of_startHotspot=JSON.stringify(res)},
  (err)=>{this.return_of_startHotspot=JSON.stringify(err)}
)
}




return_of_CHANGE_WIFI_STATE_checkper:any
access_wifistate()
{
  this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_WIFI_STATE).then(
    result =>{ console.log('Has permission?',result.hasPermission);this.return_of_CHANGE_WIFI_STATE_checkper=JSON.stringify(result)},
    err =>{ this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.CAMERA);this.return_of_CHANGE_WIFI_STATE_checkper='error=>'+JSON.stringify(err)}
  );

}

return_of_CHANGE_WIFI_STATE_getper:any
CHANGE_WIFI_STATE_getper()
{

  this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.CHANGE_NETWORK_STATE,this.androidPermissions.PERMISSION.CHANGE_WIFI_MULTICAST_STATE]).then(
    (res)=>{this.return_of_CHANGE_WIFI_STATE_getper=JSON.stringify(res)},
    (err)=>{this.return_of_CHANGE_WIFI_STATE_getper='error=>'+JSON.stringify(err)}
  );
 
}
return_of_CHANGE_NETWORK_STATE_checkper:any
CHANGE_NETWORK_STATE_percheck()
{
  this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.CHANGE_NETWORK_STATE).then(
    result =>{ console.log('Has permission?',result.hasPermission);
    
    
    this.return_of_CHANGE_NETWORK_STATE_checkper=JSON.stringify(result)
  this.createHotspot();
  
  },
    err => {this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.CAMERA);this.return_of_CHANGE_NETWORK_STATE_checkper='error=>'+JSON.stringify(err)}
  );
}

return_of_CHANGE_NETWORK_STATE_getper:any
CHANGE_NETWORK_STATE_getper()
{
  this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.CHANGE_NETWORK_STATE]).then(
    (res)=>{this.return_of_CHANGE_NETWORK_STATE_getper=JSON.stringify(res)},
    (err)=>{this.return_of_CHANGE_NETWORK_STATE_getper='error=>'+JSON.stringify(err)}
  );
}








}

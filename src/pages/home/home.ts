import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SpeechRecognition } from '@ionic-native/speech-recognition';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {


  recording:any=false;
  constructor(public navCtrl: NavController,private speechRecognition:SpeechRecognition) 
  {
this.recording=false;
  }
    

  listen_using_speech_recog()
  {
        
  // Start the recognition process
    this.speechRecognition.startListening()
    .subscribe(
    (matches: ['feedback','Navigate']) => console.log(matches),
    (onerror) => console.log('error:', onerror)
    )
   this.recording=true;

  } 
  
  
  stop_listening()
  {
    this.recording=false;

  }

}

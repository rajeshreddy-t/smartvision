import { LoginNamePage } from './../login-name/login-name';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController, ActionSheetController } from 'ionic-angular';
import { Sim } from '@ionic-native/sim';
import { Promise } from 'q';
import { MobileAccessibility } from '@ionic-native/mobile-accessibility';
import { TextToSpeech } from '@ionic-native/text-to-speech';
import { Vibration } from '@ionic-native/vibration';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

mobile_number:any
mobile_numbers:any

talk_back:boolean;

  constructor(public navCtrl: NavController,
    private maccess:MobileAccessibility ,
    private tts: TextToSpeech,
    private vibration: Vibration,
    private actionSheetCtrl:ActionSheetController,private  toastCtrl:ToastController,private loadingCtrl:LoadingController,private sim:Sim, public navParams: NavParams) {
    //  this.detect_simcard_number();


    this.maccess.isTalkBackRunning().then(
      (success)=>{
        this.talk_back=success;
      },
      (err)=>{
         this.presentToast('ms access error is talk back running')
      }
    )

try{

  this.vibration.vibrate(1000);
     this.texttospeech('welcome to  smart vision').then(
       (success)=>{ 
                   this.start_detection(); 
                  this.presentToast('welcome message done');
                },
       (err)=>{
                this.presentToast('welcome message failed');
       }
     );
}
catch(e){
  this.start_detection();
  this.presentToast('in catch block triggering start detection'+e);
}
    //  this.sim_checkperm().then((res)=>{},(err)=>{})
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

cards:any;
simInfo:any;





sim_checkperm()
{
  return Promise((resolve, reject) => {
    this.sim.hasReadPermission().then(
      (cperm)=>{console.log("permcheck=>",JSON.stringify(cperm));    resolve('success')},
      (err)=>{console.log("error in perm in check=>",err);  this.presentToast(JSON.stringify(err)); resolve(err)}
    )
  });
}

sim_request_perm()
{
  return Promise((resolve, reject) => {


try {
  this.vibration.vibrate(2000);
this.texttospeech('I am detecting your mobile number, please grant the permission by clicking on ok ')
    .then(
      (success)=>{
        this.sim.requestReadPermission().then(
          (perm)=>{console.log('permisiion granted=>'+JSON.stringify(perm)); resolve('success')},
          (err)=>{console.log('permission not granted=>',JSON.stringify(err)); resolve(err)}
        )
      },
      (err)=>{
        this.presentToast('speech problem');
        resolve('speech problem')
      }
    )


} catch (error) {
  this.presentToast('in catch in get permission')
  this.sim.requestReadPermission().then(
      (perm)=>{console.log('permisiion granted=>'+JSON.stringify(perm)); resolve('success')},
      (err)=>{console.log('permission not granted=>',JSON.stringify(err)); resolve(err)}
    )
}
    

    
  });
}

data:any
detect_simcard_number()
{
      this.sim.getSimInfo().then(
      (info)=>{  console.log("got info =>"+JSON.stringify(info)); 
                //  this.presentToast(JSON.stringify(info))
                
                 this.simInfo = info;
                 this.cards = info.cards;
            this.cards.forEach(card => {
              this.mobile_numbers.push(card.phoneNumber)
            });


            
                  // this.presentActionSheet();
  

                  this.mobile_number=this.mobile_numbers[0];
                  this.mobile_number=this.mobile_number.tostring();
                this.maccess.isTalkBackRunning().then(
                  (success)=>{ 
                    this.vibration.vibrate(2000);
                  this.maccess.speak('Hey your mobile number is '+this.mobile_number);
                  this.maccess.speak('thank you for providing mobile number');
                  this.navCtrl.push(LoginNamePage);
                   },
                  (err)=>{
                    this.vibration.vibrate(2000);
                    this.texttospeech('Hey your mobile number is '+this.mobile_number).then(
                      (success)=>{this.texttospeech('thank you for providing mobile number');
                                    this.navCtrl.push(LoginNamePage);
                                 },
                      (err)=>{  this.presentToast('speech problem')}
                    );
                    
                  }
                )


                },
      (err)=>{console.log("got error=>"+JSON.stringify(err));this.presentToast(JSON.stringify(err))}
      );

}



start_detection()
{

  this.vibration.vibrate(1000);
  this.texttospeech('Login page');
 

  this.sim_checkperm().then((res)=>{
    if(res=='success')
    {
       this.detect_simcard_number();
    }
    else
    {
      this.showLoader('gettimg sim card permissions')
      this.presentToast('no permision to read sim')
      this.sim_request_perm().then((res)=>{
        if(res='success')
        {
          this.loading.dismiss()
          this.presentToast('permission granted')
                 this.detect_simcard_number();
        }
        else{

          this.loading.dismiss();
          this.presentToast('permission declined')
        }
      },(err)=>{

        this.presentToast('error  occured in detection')
      })
    }
  },(err)=>{})

}


next()
{

  // if(false)
  // {
  //   this.vibration.vibrate([2000,1000,2000]);
    
  //   this.maccess.speak('Hey your mobile number is '+this.mobile_number);
  //   this.maccess.speak('thank you for providing mobile number');
  //   this.navCtrl.push(LoginNamePage)
  // }
  // else
  // {
    this.presentToast("talk_back=>"+this.talk_back)
    this.texttospeech('next')
    this.texttospeech('Hey your mobile number is '+this.mobile_number).then(
      (success)=>{
        this.texttospeech('thank you for providing mobile number');
        this.vibration.vibrate(1000);           
        this.navCtrl.push(LoginNamePage);
                 },
      (err)=>{  this.presentToast('speech problem')}
    );
  // }

}

presentActionSheet() 
{
    var buttons=[]
    this.cards.forEach(card => {
      buttons.push({text:card.phoneNumber,handler:()=>{console.log("clicked number=>",card.phoneNumber); this.mobile_number=card.phoneNumber }})
    });

    // this.mobile_number='9494659268'
    buttons.push({
    text: 'Cancel',
    role: 'cancel',
    handler: () => {
      console.log('Cancel clicked');
     }} 
     );

    let actionSheet = this.actionSheetCtrl.create({
      title: 'sim cards',
      buttons: buttons
    });
 
    actionSheet.present();
  }







/**tts general  */



texttospeech(msg)
{

  return Promise((resolve,reject)=>{
    this.tts.speak(msg).then(
      (success)=>{this.presentToast(msg+success); resolve('tts success')},
      (error)=>{this.presentToast(error); resolve('tts declined')}
    )

  })
  

}
/** */




  /***toast */
  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 8000,
      position: 'bottom',
      dismissOnPageChange: true
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  /**end of toast */
  loading:any;
  /**loading */
  showLoader(msg){
    this.loading = this.loadingCtrl.create({
        content: msg
    });

    this.loading.present();
  }


  /**end of loading */
}
